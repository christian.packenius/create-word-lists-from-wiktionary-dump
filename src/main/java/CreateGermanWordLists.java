import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

public class CreateGermanWordLists {
  private final Path destinationDirectory = Path.of("your path to 'german word lists' directory");
  private final Path wiktionaryDump = Path.of("your path to 'dewiktionary-JJJJMMDD-pages-articles.xml' file");

  private int linesCount, pagesCount;
  private String wort, grundform, sprache, wortart, schreibweise;
  private final List<String> worte = new ArrayList<>();
  private final Set<String> grundformen = new HashSet<>();
  private final Set<String> sprachen = new HashSet<>();
  private final Map<String, Set<String>> wortarten = new HashMap<>();

  public static void main(String[] args) throws IOException {
    LocalDateTime start = LocalDateTime.now();
    new CreateGermanWordLists();
    LocalDateTime end = LocalDateTime.now();
    System.out.println("Dauer: " + Duration.between(start, end).getSeconds() + " Sekunden.");
  }

  CreateGermanWordLists() throws IOException {
    workInput();
    showResults();
    createLists();
  }

  private void workInput() throws IOException {
    try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(wiktionaryDump.toFile())))) {
      workLinesFromInputStream(in);
    }
  }

  private void workLinesFromInputStream(BufferedReader in) throws IOException {
    String line;
    while ((line = in.readLine()) != null) {
      linesCount++;
      workNextLine(line);
    }
  }

  private void workNextLine(String line) {
    line = line.trim();
    if (line.equals("</page>")) {
      pagesCount++;
      workedStoredPageData();
    }
    if (line.startsWith("<title>") && line.endsWith("</title>")) {
      wort = line.substring(7, line.length() - 8);
    }
    if (wort != null && !wort.startsWith("MediaWiki:")) {
      workPageLine(line);
    }
  }

  private void workedStoredPageData() {
    if (!wort.contains(":") && "Deutsch".equals(sprache)) {
      workGermanWord();
    }
    clearPageData();
  }

  private void workGermanWord() {
    // Sonderfall: Andere Schreibweise.
    if (schreibweise != null) {
      worte.add(wort + " --> " + schreibweise);
      return;
    }

    StringBuilder wordParts = new StringBuilder(wort);

    wordParts.append(" | ");
    if (grundform != null) {
      wordParts.append(grundform);
      grundformen.add(grundform);
    } else {
      wordParts.append(wort);
    }

    if (wortart != null) {
      wortarten.get(wortart).add(wordParts.toString());
    }

    wordParts.append(" | ");
    if (wortart != null) {
      wordParts.append(wortart);
    }
    worte.add(wordParts.toString());
  }

  private void clearPageData() {
    wort = null;
    grundform = null;
    sprache = null;
    wortart = null;
    schreibweise = null;
  }

  private void workPageLine(String line) {
    workSprache(line);
    if ("Deutsch".equals(sprache)) {
      workWortart(line);
      workGrundformverweis(line);
      workAndereSchreibweise(line);
    }
  }

  private void workSprache(String line) {
    String[] parts = getDoubleBracketsParts(line);
    if (parts == null || parts.length <= 1) {
      return;
    }
    if (!parts[0].equalsIgnoreCase("Sprache")) {
      return;
    }
    sprache = parts[1];
    if ("deutsch".equals(sprache)) {
      sprache = "Deutsch";
    }
    if (sprache != null && !sprache.isEmpty()) {
      if (Character.isAlphabetic(sprache.charAt(0))) {
        sprachKorrektur();
        sprachen.add(sprache);
      }
    }
  }

  private void sprachKorrektur() {
    if (sprache.startsWith("Klassisches Nahuatl") && sprache.length() == "Klassisches Nahuatl".length() + 1) {
      sprache = "Klassisches Nahuatl";
    }
  }

  private void workWortart(String line) {
    String[] parts = getDoubleBracketsParts(line);
    if (parts == null || parts.length <= 1) {
      return;
    }
    if (!parts[0].equalsIgnoreCase("Wortart")) {
      return;
    }
    wortart = parts[1];
    if (wortart.endsWith("(Deutsch)")) {
      wortart = wortart.substring(0, wortart.length() - "(Deutsch)".length()).trim();
    }
    if (!wortarten.containsKey(wortart)) {
      wortarten.put(wortart, new HashSet<>());
    }
  }

  private void workGrundformverweis(String line) {
    String[] parts = getDoubleBracketsParts(line);
    if (parts == null || parts.length <= 1) {
      return;
    }
    if (!parts[0].startsWith("Grundformverweis ")) {
      return;
    }
    for (int i = parts.length - 1; i >= 1; i--) {
      if (!parts[i].contains("=")) {
        grundform = parts[i].trim();
        break;
      }
    }
  }

  private void workAndereSchreibweise(String line) {
    String[] parts = getDoubleBracketsParts(line);
    if (parts == null || parts.length <= 1) {
      return;
    }
    if (!parts[0].endsWith("Schreibweise")) {
      return;
    }
    schreibweise = parts[1];
  }

  private String[] getDoubleBracketsParts(String line) {
    int k = line.indexOf("{{") + 2;
    if (k < 2) {
      return null;
    }
    int m = line.indexOf("}}", k);
    if (m < 0) {
      return null;
    }
    String[] parts = line.substring(k, m).split("\\|");
    for (int i = parts.length - 1; i >= 0; i--) {
      parts[i] = parts[i]
        .replace("&amp;", "&")
        .replace("&gt;", ">")
        .replace("&lt;", "<")
        .replace("&quot;", "\"")
        .replace("&nbsp;", " ")
        .replace("&ensp;", " ")
        .replace("&emsp;", " ")
        .replace("&hellip;", "…")
        .replace("&#39;", "'")
        .replace("&#61;", "=")
        .replace("&#91;", "[")
        .replace("&#93;", "]")
        .replace("&#935;", "Х")
        .replace("&#1061;", "Х")
        .replace("&#1615;", "*")
        .replace("&#1616;", "*")
        .replace("&#8209;", "-")
        .replace("&amp;", "&")
        .trim();
    }
    return parts;
  }


  private void showResults() {
    System.out.println("Wiktionary Dump: " + wiktionaryDump.toAbsolutePath());
    System.out.printf("-> %,d Zeilen mit %,d HTML-Seiten.\r\n", linesCount, pagesCount);
    System.out.printf("-> %,d deutsche Worte.\r\n", worte.size());
    System.out.printf("-> %,d Grundformen.\r\n", grundformen.size());
    System.out.printf("-> %,d Sprachen.\r\n", sprachen.size());
    System.out.printf("-> %,d Wortarten.\r\n", wortarten.size());
  }

  private void createLists() throws IOException {
    if (!Files.exists(destinationDirectory)) {
      Files.createDirectories(destinationDirectory);
    }

    sortAndStoreStringSet(sprachen, "de - Sprachen.txt");
    sortAndStoreStringSet(wortarten.keySet(), "de - Wortarten.txt");
    sortAndStoreStringSet(grundformen, "de - Grundformen.txt");
    for (String wortart : wortarten.keySet()) {
      sortAndStoreStringSet(wortarten.get(wortart), "de - Wortart '" + wortart + "'.txt");
    }
    sortAndStoreStringList(worte, "de - Worte.txt");
  }

  private void sortAndStoreStringSet(Set<String> stringSet, String filename) throws IOException {
    List<String> stringList = new ArrayList<>(stringSet);
    sortAndStoreStringList(stringList, filename);
  }

  private void sortAndStoreStringList(List<String> stringList, String filename) throws IOException {
    stringList.sort(String.CASE_INSENSITIVE_ORDER);
    Files.write(destinationDirectory.resolve(filename), stringList);
  }
}
