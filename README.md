# create-word-lists-from-wiktionary-dump

Mithilfe dieses Projektes können aus einem Wiktionary-Dump (also
einem regelmäßigen Abzug sämtlicher Einträge) diverse Wortlisten
erstellt werden. Mit nur wenig Programmieraufwand und Kenntnissen
der entsprechenden Sprache kannst du auch Wortlisten für andere
Sprachen erstellen. Ebenfalls sollte es kein Problem sein, auch
andere Wortlisten zu erstellen, falls du diese benötigen solltest.

## Dump-File

Die Wikimedia erstellt regelmäßig mehrere verschiedene Abzüge
sämtlicher Wikis aller Sprachen. Diese sind über den folgenden
Link zentral erreichbar:

https://dumps.wikimedia.org/

Für dieses Projekt navigiere von hier aus zum ersten Link
"Database backup dumps". Suche dann über die Browsersuche nach
*dewiktionary* und folge diesem Dump weiter. Er führt dich zu
einer Seite, in der das Datum mit eingeflochten ist:

https://dumps.wikimedia.org/dewiktionary/JJJJMMDD/

Das von diesem Projekt verwendete Backup findest du entweder
über den Link, bei dem du das gleiche Datum wie oben einbaust

https://dumps.wikimedia.org/dewiktionary/20240420/dewiktionary-JJJJMMDD-pages-articles.xml.bz2

oder indem du nach *-pages-articles.xml.bz2* suchst. Er steht in
einem Kasten mit der folgenden Überschrift:

*Articles, templates, media/file descriptions, and primary meta-pages.*

Zum aktuellen Zeitpunkt ist diese Datei etwa 210MB groß. Du kannst sie
mittels WinRAR, 7zip oder einem anderen Programm entpacken. Heraus purzelt
eine Datei namens *dewiktionary-JJJJMMDD-pages-articles.xml* in der Größe
von etwa 1,9GB. Diese Datei enthält nun sämtlicher Seiten der deutschsprachigen
Wiktionary.

Den Pfad auf diese Datei und den Pfad auf ein Verzeichnis für die Wortlisten
stellst du in der Klasse ein und startest das Programm. Je nach Geschwindigkeit
des Rechners und der Festplatte dauert der Lauf zwischen weniger Sekunden und
wenigen Minuten.

Ursprünglich wurde das Programm mittels SAX-Parser geschrieben; aus
Geschwindigkeitsgründen habe ich es so umgeschrieben, dass die Datei schlicht
zeilenweise eingelesen und interpretiert wird.

## Bisher erstellte Dateien

- **de - Sprachen.txt** enthält sämtliche in der Datei gefundenen Sprachen. \
  Alle folgenden Dateien enthalten nur Worte, die mit "Deutsch" 
  gekennzeichnet sind. 
- **de - Grundformen.txt** enthält nur Worte in ihrer Grundform.
- **de - Wortarten.txt** listet alle gefundenen Wortarten auf.
- **de - Wortart 'XYZ'.txt** listet sämtliche Worte der Wortart XYZ auf, es 
  gibt also für jede Wortart eine eigene Datei. Hier findet man in jeder 
  Zeile links vom '|' das eigentliche Wort und rechts vom '|' seine Grundform.
- **de - Worte.txt** enthält sämtliche Worte aus dem Wiktionary-Dump, wobei 
  es je Zeile eine von zwei Formen gibt:
  - Die *normalen* Zeilen enthalten drei mit '|' getrennte Teile: Links das 
    eigentliche Wort, in der Mitte seine Grundform und rechts dessen Wortart.
  - In anderen Zeilen gibt es einen Pfeil '-->'; diese enthalten links das 
    Wort in einer alten/anderen/fehlerhaften Schreibweise und rechts die 
    korrekte Schreibweise.

Wie gesagt: Es ist recht einfach möglich, das Programm für weitere Listen 
zu erweitern oder aber andere Sprach-Wiktionary-Dumps zu parsen.

